package lesson50homework.microgram.Controller;

import lesson50homework.microgram.Model.*;
import lesson50homework.microgram.Services.PublicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
public class PublicationController {
    @Autowired
    PublicationRepository publicationRepository;

    private final PublicationService publicationService;

    public PublicationController(PublicationService publicationService) {
        this.publicationService = publicationService;
    }


    @GetMapping("/subscription_publications/{userId}")
    public List<Publication> findPublicationsOfOtherUsers(@PathVariable("userId") String userId){
        return publicationRepository.getPublicationsByUserIdNot(userId);
    }

    @GetMapping("/publications_of_subscriptions/{userId}")
    public Set<Publication> findPublicationsOfMySubscriptions(@PathVariable("userId") String userId){
        return publicationService.getPublications(userId);
    }
}