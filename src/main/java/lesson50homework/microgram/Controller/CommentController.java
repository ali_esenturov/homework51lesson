package lesson50homework.microgram.Controller;

import lesson50homework.microgram.Model.CommentRepository;
import lesson50homework.microgram.Services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommentController {
    @Autowired
    CommentRepository commentRepository;

    final private CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }
}
